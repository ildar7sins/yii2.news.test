<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "similar_news".
 *
 * @property int $news_id
 * @property int $similar_news_id
 *
 * @property News $news
 * @property News $similarNews
 */
class SimilarNews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'similar_news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'similar_news_id'], 'required'],
            [['news_id', 'similar_news_id'], 'integer'],
            [['news_id', 'similar_news_id'], 'unique', 'targetAttribute' => ['news_id', 'similar_news_id']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
            [['similar_news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['similar_news_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'similar_news_id' => 'Similar News ID',
        ];
    }

    /**
     * Gets query for [[News]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * Gets query for [[SimilarNews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSimilarNews()
    {
        return $this->hasOne(News::className(), ['id' => 'similar_news_id']);
    }
}
