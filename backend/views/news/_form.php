<?php

use yii\helpers\Url;
use yii\helpers\Html;
use backend\models\City;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use trntv\filekit\widget\Upload;


/**
 * @var yii\web\View $this
 * @var backend\models\News $model
 * @var yii\bootstrap4\ActiveForm $form
 */

$data = ArrayHelper::map($model->similar, 'id', 'title');
?>

<div class="news-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?= $form->errorSummary($model); ?>

                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'row' => 3]) ?>
				<?= $form->field($model, 'body')->widget(
					\yii\imperavi\Widget::class,
					[
						'plugins' => ['fullscreen', 'fontcolor'],
						'options' => [
							'minHeight' => 400,
							'maxHeight' => 400,
							'buttonSource' => true,
							'convertDivs' => false,
							'removeEmptyTags' => true,
							'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
						],
					]
				) ?>
				<?= $form->field($model, 'thumbnail')->widget(
					Upload::class,
					[
						'url' => ['/file/storage/upload'],
						'maxFileSize' => 5000000, // 5 MiB,
						'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
					]
				) ?>


                <?= $form->field($model, 'city')->widget(Select2::classname(), [
					'data' => ArrayHelper::map(City::find()->asArray()->all(), 'id', 'name'),
					'language' => 'ru',
					'options' => ['placeholder' => 'Выбрать...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]) ?>
                <?= $form->field($model, 'similar')->widget(Select2::classname(), [
					'data' => $data,
					'language' => 'ru',
					'options' => [
					    'placeholder' => 'Выбрать...',
                        'multiple' => true,
                    ],
					'pluginOptions' => [
						'minimumInputLength' => 3,
						'allowClear' => true,
						'maximumSelectionLength' => 3,
						'ajax' => [
							'url' => Url::to(['list']),
							'dataType' => 'json',
							'data' => new JsExpression('function(params) { return {q:params.term}; }')
						],
						'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
						'templateResult' => new JsExpression('function(item) { return item.title; }'),
						'templateSelection' => new JsExpression('function (item) { return item.title || item.text; }'),
					],
				]) ?>

				<?php echo $form->field($model, 'status')->checkbox() ?>
                
            </div>
            <div class="card-footer">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
