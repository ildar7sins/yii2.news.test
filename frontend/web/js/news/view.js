
const NewsView = new Vue({
    el: "#news-view",
    components: {
        'news-card': NewsCard
    },
    data() {
        return {
            news: news
        };
    },
    methods: {
        async toggleFavorite(){
            const result = await apiPatch(`/rest/news/toggle-favorite?id=${this.news.id}`);

            if(result) {
                this.news.favorite = !this.news.favorite;
            }
        },
    }
});
