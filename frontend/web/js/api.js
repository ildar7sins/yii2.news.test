function errrorHandler(e) {
    console.error(e);

    return false;
}

const apiPost = async (url, data = {}) => {
    try {
        const response = await axios.post(url, data);

        return response.data;
    } catch (e) {
        errrorHandler(e);
    }
}

const apiGet = async (url, data) => {
    if (data) {
        let getParams = await Object.keys(data).map(function (key) {
            return key + '=' + data[key];
        }).join('&');

        url += `?${getParams}`
    }

    try {
        const response = await axios.get(url);

        return response.data;
    } catch (e) {
        errrorHandler(e);
    }
}

const apiDelete = async (url, data) => {
    if (data) {
        let getParams = await Object.keys(data).map(function (key) {
            return key + '=' + data[key];
        }).join('&');

        url += `?${getParams}`
    }

    try {
        const response = await axios.delete(url);

        return response.data;
    } catch (e) {
        errrorHandler(e);
    }
}

const apiPatch = async (url, data) => {
    try {
        const response = await axios.patch(url, data);
        return response.data;
    } catch (e) {
        errrorHandler(e);
    }
}

const apiPut = async (url, data) => {
    try {
        const response = await axios.put(url, data);
        return response.data;
    } catch (e) {
        errrorHandler(e);
    }
}

