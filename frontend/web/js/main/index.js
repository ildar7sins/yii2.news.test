
const DocumentPage = new Vue({
    el: "#main-page",
    components: {
        'news-card': NewsCard
    },
    data() {
        return {
            currentCity: cities[0].id,
            favorite: false,
            cities: cities,
            news: null
        };
    },
    methods: {
        toggleFavorite(){
            this.favorite = !this.favorite;
            this.getNews();
        },
        changeCity(event) {
            this.currentCity = event.target.value;
            this.getNews();
        },
        async getNews() {
            const result = await apiGet('/rest/news/index', {
                favorite: this.favorite ? 1 : 0,
                city: this.currentCity
            });

            if(result) {
                this.news = result.data;
            }
        },

    },
    async created() {
        this.getNews();
    }
});
