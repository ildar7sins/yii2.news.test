<?php

namespace frontend\controllers\rest;

use backend\models\FavoriteNews;
use frontend\resources\search\NewsSearch;
use frontend\resources\News;

/**
 * Order controller
 * @author Ildar Gaskarov <ildar7sins@gmail.com>
 */
class NewsController extends \yii\rest\Controller {

	public string $modelClass = News::class;

	public function actionIndex() {
		News::setSerializeType(News::FIELDS_SIMPLE);
		$documentSearch = new NewsSearch();

		return $documentSearch->search($_GET);
	}

	/**
	 * @param $id
	 * @return string
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionToggleFavorite($id) {
		$model = FavoriteNews::find()
			->where([
				'user_id' => \Yii::$app->user->id,
				'news_id' => $id
			])
			->one();

		if(is_null($model)){
			$model = new FavoriteNews();
			$model->user_id = \Yii::$app->user->id;
			$model->news_id = $id;
			$model->save();
		} else {
			$model->delete();
		}

		return 'OK';
	}

	/**
	 * Поиск модели по id...
	 * @param $id
	 * @return News|null
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id) {
		if (($model = News::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Документ не найден.');
		}
	}
}
