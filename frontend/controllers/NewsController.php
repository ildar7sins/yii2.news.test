<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\resources\News;
use yii\web\NotFoundHttpException;

class NewsController extends Controller {

	/**
	 * @param $id
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionView($id) {
		$model = $this->findModel($id);
		return $this->render('view', ['model' => $model]);
	}

	/**
	 * Поиск модели по id...
	 * @param $id
	 * @return News|null
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id) {
		if (($model = News::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Документ не найден.');
		}
	}
}
