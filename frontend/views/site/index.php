<?php
/**
 * @var yii\web\View $this
 */
$this->title = Yii::$app->name;

use backend\models\City;
use frontend\assets\FrontendAsset;
use common\extensions\AssetsRegister;

$this->registerJsVar('cities', City::find()->asArray()->all());
AssetsRegister::registerViewAssets([FrontendAsset::class], '@frontendUrl', $this, ['/js/main/index.min.js']);
?>
<div id="main-page" class="page-content">
    <div class="container pt-5 pb-5">
        <h3 class="text-center" v-if="news === null">Загрузка...</h3>
        <div class="row" v-else>
            <div class="col-12 mb-3 d-flex flex-row justify-content-end">
                <select class="transparent-select" v-model="currentCity" @change="changeCity($event)">
                    <option v-for="city in cities" :value="city.id" :key="city.id">
                        {{city.name}}
                    </option>
                </select>
                <button class="btn btn-link" @click="toggleFavorite">
                    <template v-if="favorite">
                        <i class="fas fa-2x fa-star"></i>
                    </template>
                    <template v-else>
                        <i class="far fa-2x fa-star"></i>
                    </template>
                </button>
            </div>
            <div class="col-12 col-md-4" v-for="item in news">
                <news-card :news="item"/>
            </div>
        </div>
    </div>
</div>
