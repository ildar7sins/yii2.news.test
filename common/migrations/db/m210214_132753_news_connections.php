<?php

use yii\db\Migration;

/**
 * Class m210214_132753_news_connections
 */
class m210214_132753_news_connections extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		// Favorite news...
		$this->createTable('favorite_news', [
			'user_id' => $this->integer()->notNull(),
			'news_id' => $this->integer()->notNull(),
		]);
		$this->addForeignKey('fk-favorite_news-user_id', 'favorite_news', 'user_id',
			'user', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addForeignKey('fk-favorite_news-news_id', 'favorite_news', 'news_id',
			'news', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addPrimaryKey('favorite_news_pk', 'favorite_news', ['user_id', 'news_id']);
		
		
		// City news...
		$this->createTable('city_news', [
			'city_id' => $this->integer()->notNull(),
			'news_id' => $this->integer()->notNull(),
		]);
		$this->addForeignKey('fk-city_news-city_id', 'city_news', 'city_id',
			'city', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addForeignKey('fk-city_news-news_id', 'city_news', 'news_id',
			'news', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addPrimaryKey('city_news_pk', 'city_news', ['city_id', 'news_id']);
		
		
		// Similar news...
		$this->createTable('similar_news', [
			'news_id' => $this->integer()->notNull(),
			'similar_news_id' => $this->integer()->notNull(),
		]);
		$this->addForeignKey('fk-similar_news-news_id', 'similar_news', 'news_id',
			'news', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addForeignKey('fk-similar_news-similar_news_id', 'similar_news', 'similar_news_id',
			'news', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addPrimaryKey('similar_news_pk', 'similar_news', ['news_id', 'similar_news_id']);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		// Favorite news...
		$this->dropForeignKey('fk-favorite_news-user_id', 'favorite_news');
		$this->dropForeignKey('fk-favorite_news-news_id', 'favorite_news');
		$this->dropTable('favorite_news');
		
		
		// City news...
		$this->dropForeignKey('fk-city_news-city_id', 'city_news');
		$this->dropForeignKey('fk-city_news-news_id', 'city_news');
		$this->dropTable('city_news');
		
		
		// Similar news...
		$this->dropForeignKey('fk-similar_news-news_id', 'similar_news');
		$this->dropForeignKey('fk-similar_news-similar_news_id', 'similar_news');
		$this->dropTable('similar_news');
	}
	
}
