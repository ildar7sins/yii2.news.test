<?php

use yii\db\Migration;

/**
 * Class m210214_131935_news
 */
class m210214_131935_news extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('news', [
			'id' => $this->primaryKey(),
			'title' => $this->string(512)->notNull(),
			'description' => $this->string(512)->notNull(),
			'body' => $this->text()->notNull(),
			'thumbnail_base_url' => $this->string(1024),
			'thumbnail_path' => $this->string(1024),
			'status' => $this->tinyInteger()->notNull()->defaultValue(0),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
		]);
		
		$this->addForeignKey('fk-news-created_by', 'news', 'created_by',
			'user', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addForeignKey('fk-news-updated_by', 'news', 'updated_by',
			'user', 'id', 'CASCADE', 'CASCADE'
		);
		
		$this->createIndex('idx-news-created_by', 'news', 'created_by');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropForeignKey('fk-news-created_by', 'news');
		$this->dropForeignKey('fk-news-updated_by', 'news');
		
		$this->dropIndex('idx-news-created_by', 'news');
		
		$this->dropTable('news');
	}
}
