<?php

use yii\db\Migration;

/**
 * Class m210214_130623_city
 */
class m210214_130623_city extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->createTable('city', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255)->notNull()
		]);
		
		$this->insert('city', ['id' => 1, 'name' => 'Уфа']);
		$this->insert('city', ['id' => 2, 'name' => 'Москва']);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropTable('city');
	}
}
