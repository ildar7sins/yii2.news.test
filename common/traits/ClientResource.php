<?php

namespace common\traits;

use yii\helpers\BaseInflector;

trait ClientResource {
	static $serializeType = 'DEFAULT';
	public $modelType = 'DEFAULT';

	/**
	 * @inheritdoc
	 */
	public function afterFind() {
		parent::afterFind();

		$this->modelType = self::$serializeType;
	}

	public static function setSerializeType(string $type): void {
		self::$serializeType = $type;
	}

	public function load($data, $formName = null) {
		$data = self::prepareLoadData($data);

		return parent::load($data, $formName);
	}

	public function getBaseArray(array $fields = [], array $expand = [], $recursive = true) {
		return parent::toArray($fields, $expand, $recursive);
	}

	public function toArray(array $fields = [], array $expand = [], $recursive = true) {
		$model = parent::toArray($fields, $expand, $recursive);

		return self::prepareResourseData($model);
	}

	private function prepareLoadData(array $data) {
		$result = [];
		foreach ($data as $key => $value) {
			$result[BaseInflector::underscore($key)] = $value;
		}
		return $result;
	}

	private function prepareResourseData(array &$data) {
		$result = [];
		foreach ($data as $key => $value) {
			$result[BaseInflector::variablize($key)] = $value;
		}
		return $result;
	}

	/**
	 * Загрузка параметров для модели поиска...
	 * @param array $params
	 */
	public function loadSearchParams(array $params) {
		if (isset($params['query']) && $params['query'] && is_string($params['query'])) {
			$params = array_merge($params, json_decode($params['query'], true));
		}

		$this->load($params, '');
	}
}
