# yii2.news.test

- Страницы всех новостей и отдельной новости (новость состоит из изображения, заголовка, краткого описания, текста)
- Раздел "Похожие новости" на странице отдельной новости. Похожие новости присваиваются в административной панели к каждой новости отдельно.
- Переключение между двумя городами. К каждому городу привязаны определенные новости.
- Избранные новости на главной, выбираемые пользователем (у каждой новости есть кнопка "Добавить в избранное", работающая без перезагрузки страницы).
- Возможность сохранения всех новостей из административной панели в формате Excel по нажатию на кнопку.
